package com.kt.mvc.carSalon.controller.carSalonController;

import com.kt.mvc.carSalon.controller.carCreatorController.CarCreatorController;
import com.kt.mvc.carSalon.controller.printerController.PrinterMVC;
import com.kt.mvc.carSalon.controller.scannerController.ScannerMVC;
import com.kt.mvc.carSalon.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.mockito.Mockito.*;

public class CarSalonControllerTest {

    Person person;
    Car car;
    PrinterMVC.Controller printer;
    ScannerMVC.Controller scanner;
    CarCreatorController carCreatorController;
    CarSalonControllerMVC.Controller tested;

    @BeforeEach
    public void setup(){
        person = new Person();
        person.addMoney(10000000);

        car = new Car();
        car.setBodyType(BodyType.NONE);
        car.setBrand(Brand.NONE);
        car.setColour(Colour.NONE);
        car.setFuelType(FuelType.NONE);
        car.setUpholstery(Upholstery.NONE);

        printer = mock(PrinterMVC.Controller.class) ;
        scanner = mock(ScannerMVC.Controller.class);
        carCreatorController = mock(CarCreatorController.class);
        tested = new CarSalonController(carCreatorController,scanner,person,printer);
    }

    @Test
    public void shouldShowMenuWhenControllerLaunched(){
        //given
        when(scanner.pickOption()).thenReturn(6);
        //when
        tested.configure();
        //then
        verify(printer,times(9)).println(anyString());
    }

    @Test
    public void shouldPickColourWhenOptionSelected(){
        //given
        when(scanner.pickOption()).thenReturn(1,0).thenReturn(6);
        //when
        tested.configure();
        //then
        verify(carCreatorController, times(1)).pickColor();
    }

    @Test
    public void shouldPickFuelTypeWhenOptionSelected(){
        //given
        when(scanner.pickOption()).thenReturn(2,0).thenReturn(6);
        //when
        tested.configure();
        //then
        verify(carCreatorController, times(1)).pickFuelType();
    }

    @Test
    public void shouldPickBrandWhenOptionSelected(){
        //given
        when(scanner.pickOption()).thenReturn(3,0).thenReturn(6);
        //when
        tested.configure();
        //then
        verify(carCreatorController, times(1)).pickBrand();
    }

    @Test
    public void shouldPickBodyTypeWhenOptionSelected(){
        //given
        when(scanner.pickOption()).thenReturn(4,0).thenReturn(6);
        //when
        tested.configure();
        //then
        verify(carCreatorController, times(1)).pickBodyType();
    }

    @Test
    public void shouldPickUpholsteryWhenOptionSelected(){
        //given
        when(scanner.pickOption()).thenReturn(5,0).thenReturn(6);
        //when
        tested.configure();
        //then
        verify(carCreatorController, times(1)).pickUpholstery();
    }

    @Test
    public void shouldEndProgramWhenOptionSelected(){
        //given
        when(scanner.pickOption()).thenReturn(6,0);
        //when
        tested.configure();
        //then
        verifyZeroInteractions(carCreatorController);

    }

    @Test
    public void shouldShowWalletSaldoWhenOptionSelected(){
        //given
        when(scanner.pickOption()).thenReturn(7,0).thenReturn(6);
        //when
        tested.configure();
        //then
        verify(printer, times(1)).println(person.getCurrentWallet());

    }

    @Test
    public void shouldShowCarWhenOptionSelected(){
        //given
        when(scanner.pickOption()).thenReturn(8,0).thenReturn(6);
        //when
        tested.configure();
        //then
        verify(printer, times(1)).println(carCreatorController.returnCar());
        doReturn(car).when(carCreatorController).returnCar();
    }

    @Test
    public void shouldShowMenuWhenNoOptionSelected(){
        //given
        when(scanner.pickOption()).thenReturn(9,0).thenReturn(6);
        //when
        tested.configure();
        //then
        verifyZeroInteractions(carCreatorController);
    }
}