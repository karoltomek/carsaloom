package com.kt.mvc.carSalon.model;

import com.kt.mvc.carSalon.controller.carCreatorController.MenuInterface;
import com.kt.mvc.carSalon.controller.printerController.PrinterController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class UpholsteryTest {

    Person person;
    Person personPoor;
    Car car;
    MenuInterface menuInterface;
    PrinterController printerController;

    @BeforeEach
    public void setup(){
        person = new Person();
        personPoor = new Person();
        personPoor.addMoney(0);
        person.addMoney(1000000);

        car = new Car();
        car.setBrand(Brand.NONE);
        car.setBrand(Brand.NONE);
        car.setUpholstery(Upholstery.NONE);
        car.setUpholstery(Upholstery.NONE);
        car.setUpholstery(Upholstery.NONE);

        menuInterface = mock(MenuInterface.class);
        printerController = mock(PrinterController.class);
    }


    @ParameterizedTest
    @EnumSource(value = Upholstery.class, names = {"SKORA", "SKORA_PIKOWANA"})
    public void shouldNotAllowToSelectOptionWhenPersonHaveNotEnoughtMoney(Upholstery upholstery) {
        //when
        upholstery.checkPickedUpholstery(personPoor, car, menuInterface, printerController);
        //then
        assertAll(
                () -> verify(menuInterface).goToUpholsteryPick(),
                () -> verify(printerController).println(anyString())
        );
    }

    @ParameterizedTest
    @EnumSource(value = Upholstery.class, names = {"SKORA", "SKORA_PIKOWANA"})
    public void shouldAllowToSelectOptionWhenPersonHaveEnoughtMoney(Upholstery upholstery) {
        //when
        upholstery.checkPickedUpholstery(person, car, menuInterface, printerController);
        //then
        assertAll(
                () -> assertEquals(person.getCurrentWallet(), 1000000 - upholstery.getPrice()),
                () -> assertEquals(car.getUpholstery(), upholstery),
                () -> verifyNoMoreInteractions(printerController),
                () -> verifyNoMoreInteractions(menuInterface)
        );
    }

    @ParameterizedTest
    @EnumSource(value = Upholstery.class, names = {"WELUR"})
    public void shouldAllowToSelectFreeOptionWhenPersonSelectIt(Upholstery upholstery) {
        //when
        upholstery.checkPickedUpholstery(personPoor, car, menuInterface, printerController);
        upholstery.checkPickedUpholstery(person, car, menuInterface, printerController);
        //then
        assertAll(
                () -> assertEquals(person.getCurrentWallet(), 1000000 - upholstery.getPrice()),
                () -> assertEquals(car.getUpholstery(), upholstery),
                () -> verifyNoMoreInteractions(printerController),
                () -> verifyNoMoreInteractions(menuInterface)
        );
    }
}