package com.kt.mvc.carSalon;

import com.kt.mvc.carSalon.view.CarSalonView;

class Main {

    public static void main(String[] args) {
        new CarSalonView().start();
    }
}
