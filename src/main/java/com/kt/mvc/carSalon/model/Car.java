package com.kt.mvc.carSalon.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Car {

    private Colour colour;
    private FuelType fuelType;
    private BodyType bodyType;
    private Brand brand;
    private Upholstery upholstery;

    @Override
    public String toString() {
        return "WYBRANY SAMOCHÓD: " +
                "KOLOR: " + colour +
                ", PALIWO: " + fuelType +
                ", NADWOZIE: " + bodyType +
                ", MARKA: " + brand +
                ", TAPICERKA: " + upholstery +
                '.';
    }
}
