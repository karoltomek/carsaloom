package com.kt.mvc.carSalon.model;

import com.kt.mvc.carSalon.controller.printerController.PrinterController;
import com.kt.mvc.carSalon.controller.scannerController.ScannerController;
import com.kt.mvc.carSalon.controller.printerController.PrinterMVC;
import com.kt.mvc.carSalon.controller.scannerController.ScannerMVC;

public class Person {

    private int currentWallet;
    private ScannerMVC.Controller scannerController = new ScannerController();
    private PrinterMVC.Controller printer = new PrinterController();


    public int getCurrentWallet() {
        return currentWallet;
    }

    public void subtractMoney (int money) {
        this.currentWallet -= money;
    }

    public void addMoney (int money) {
        this.currentWallet += money;
    }

    public void createWallet () {
        printer.println("Podaj ilość gotówki");
        this.currentWallet += scannerController.nextInt();
    }

}
