package com.kt.mvc.carSalon.controller.carSalonController;

public interface CarSalonControllerMVC {
    interface Controller{
        void configure();
    }
}
