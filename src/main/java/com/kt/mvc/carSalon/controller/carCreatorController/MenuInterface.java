package com.kt.mvc.carSalon.controller.carCreatorController;

public interface MenuInterface {

    void goToColourPick();
    void goToFuelTypePick();
    void goToBrandPick();
    void goToBodyTypePick();
    void goToUpholsteryPick();
}
