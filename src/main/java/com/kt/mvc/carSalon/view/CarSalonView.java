package com.kt.mvc.carSalon.view;

import com.kt.mvc.carSalon.controller.carCreatorController.CarCreatorController;
import com.kt.mvc.carSalon.controller.carSalonController.CarSalonController;
import com.kt.mvc.carSalon.controller.printerController.PrinterController;
import com.kt.mvc.carSalon.controller.scannerController.ScannerController;
import com.kt.mvc.carSalon.controller.carCreatorController.CarCreatorControllerMVC;
import com.kt.mvc.carSalon.controller.carSalonController.CarSalonControllerMVC;
import com.kt.mvc.carSalon.controller.printerController.PrinterMVC;
import com.kt.mvc.carSalon.model.Car;
import com.kt.mvc.carSalon.model.Person;
import com.kt.mvc.carSalon.controller.scannerController.ScannerMVC;

public class CarSalonView  {

    private ScannerMVC.Controller scannerMVC = new ScannerController();
    private Person person = new Person();
    private CarCreatorControllerMVC.Controller carCreatorControllerMVC;
    private CarSalonControllerMVC.Controller carSalonController;
    private PrinterMVC.Controller printer = new PrinterController();



    public void start() {
        carCreatorControllerMVC = new CarCreatorController(scannerMVC, new Car(), person, printer);
        person.createWallet();
        carSalonController = new CarSalonController(carCreatorControllerMVC, scannerMVC, person, printer);
        carSalonController.configure();
    }
}

